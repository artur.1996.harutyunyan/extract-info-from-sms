<?php


class ExtractInfoFromSmsHelper
{
    /**
     * @var string
     */
    public static $PRICE_REGEX_FOR_FUN_PAY_EMULATOR = '/(\d+(\,\d+)?)(р\.?)/';
    /**
     * @var string
     */
    public static $RIGHT_PRICE_REGEX = '/\d{1,3}(?:[.,]\d{3})*(?:[.,]\d{2})(р\.?)/';
    /**
     * @var string
     */
    public static $CODE_REGEX  = '/\D(\d{4})\s/';
    /**
     * @var string
     */
    public static $CREDIT_CARD_REGEX = '/\b(?:\d[ -]*?){13,16}\b/';

    /**
     * @param string $text
     * @return mixed|null
     */
    public static function extractCode(string $text)
    {
        preg_match(self::$CODE_REGEX, $text, $match);

        if (isset($match[1])) {
            return $match[1];
        }

        return null;
    }

    /**
     * @param $text
     * @return mixed|null
     */
    public static function extractPriceForFunPayEmulator(string $text)
    {
        preg_match(self::$PRICE_REGEX_FOR_FUN_PAY_EMULATOR, $text, $match);

        if (isset($match[1])) {
            return $match[1];
        }

        return null;
    }

    /**
     * @param string $text
     * @return int|string|null
     * @throws Exception
     */
    public static function extractPrice(string $text)
    {
        preg_match(self::$RIGHT_PRICE_REGEX, $text, $match);

        if (isset($match[1])) {
            return $match[1];
        }

        return null;
    }

    /**
     * @param string $text
     * @return mixed|null
     */
    public static function extractCreditCard(string $text)
    {
        preg_match(self::$CREDIT_CARD_REGEX, $text, $match);

        if (isset($match[1])) {
            return $match[1];
        }

        return null;
    }
}